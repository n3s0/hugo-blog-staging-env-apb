## Hugo Blog Dev Environment Ansible Setup

### Introduction

That's kind of a mouthfull. Will need to work on a better name later. This playbook is something that I've hashed together while learning Ansible. Doing the same thing over and over again can be a tedious process. Although, setting up a Hugo environment for my current blog doesn't require a lot of steps, the goal is to achieve the ultimate threshhold of laziness I could.

Note that this would also work with other's workfow should they have a Static site with a similar environment. You would just need to edit the ```group_vars/all``` file to reflect the appropriate Git repositories and submodules. I can assist should there be questions. May not reply right away though. I have things.

### Why?

I wrote this playbook for the purpose of learning Ansible mostly. I attempted to follow the best practices available in the documentation as best I could for the file structure of Ansible playbooks. Vaults are not used for this. MMostly because I haven't wrapped my head around some things. In this case, the prompt for ```become``` is sufficient for my needs.

### What does it do?

The workflow for this playbook consists of the following. For now it has been tested locally on a workstation.

1. Install Hugo usign the hugo role.
2. Install Git git role.
3. Execute the commands in the common role. Which do the following.
  - Create the ```$HOME/Documents/sites/```directory if it doesn't already exist.
  - Clone the git repository into the ```sites``` directory.
    - [https://gitlab.com/n3s0/n3s0.gitlab.io.git](https://gitlab.com/n3s0/n3s0.gitlab.io.git)
  - Pull the git repository's submodules from remote. (This is the Hugo Hermit theme.)

### Blog Dev Environment Overview

Little overview of the Blog's environment. There really isn't any development involved. It is mosly just a Hugo instance with the Hermit template hosted on the GitLab CD/CI platform. I edit static Markdown files locally and push them using Git.

Below is the site if you would like to view it.

- [https://n3s0.gitlab.io/](https://n3s0.gitlab.io)

Below is the environment layed out that is used for editing and testing.

1. Hugo is installed to view the site.
2. Git repository is cloned into a sites directory.
3. The Git Submodule for the Hermit theme is pulled from the remote.
4. Static Markdown files are editted using Vim.
5. Web browser is used to test content locally.

### Requirements

This is a list of requirements for this Ansible playbook. If you test it on a previous/later version and it works, I can surely make updates to this. Requirements aren't technically set in stone.

- Ansible >= 2.9
- Python >= 3.7
- Wheel/Sudo privileges.

### Tested OS

Just a little overview of tested/verified operating systems. This will be tested on a case by case basis. Should there be anyone that would like to test on other distrobutions, let me know and I will add them to the list.

| Operating System | Status | Date |
|:---------------:|:-----:|:------:|
| Fedora 31 Workstation | PASS | 02/07/2020 |
| Fedora 33 Workstation | PASS | 11/18/2020 |

### Running the Playbook

To get started running the playbook you will need to do the following.

Note that I will be looking for a better way to do this.

1. Clone the git repository for the playbook.
2. Edit the ```./group_vars/all``` file and add your ```username``` and ```group``` using a text editor.
2. Run the playbook using the following command.
  ```
  ansible-playbook site.yml -i ./hosts -K
  ```
4. Enter your password for sudo.
5. Let it work.

### Problems?

Of course, if there should be any unexpected errors from Ansible I'm happy to help. Don't get discouraged if I tell you Google has the answer to your problem though.
